<?php

use Domain\Blog\Test\Adapters\InMemoryPostRepository;
use Domain\Blog\UseCase\CreatePost;
use Symfony\Component\HttpFoundation\Request;

require __DIR__.'/vendor/autoload.php';

$request = Request::createFromGlobals();

$repository = new InMemoryPostRepository();

$useCase = new CreatePost($repository);

$controller = new \App\Controller\CreatePostController($useCase);

$response = $controller->handleRequest($request);

$response->send();
