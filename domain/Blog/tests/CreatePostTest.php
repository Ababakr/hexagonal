<?php

use Domain\Blog\Entity\Post;
use Domain\Blog\Exception\InvalidDataPostException;
use Domain\Blog\Test\Adapters\InMemoryPostRepository;
use \Domain\Blog\UseCase\CreatePost;
use Domain\Blog\Test\Adapters\PDOPostRepository;

use function PHPUnit\Framework\assertInstanceOf;

it("should create a post", function() {
    $repository = new InMemoryPostRepository();
    //$repository = new PDOPostRepository();
    $useCase = new CreatePost($repository);

    $post = $useCase->execute([
        "title" => "Mon titre",
        "content" => "Mon contenu",
        "publishedAt" => new DateTime('2022-04-24 14:30:00')
    ]);

    assertInstanceOf(Post::class, $post);
    \PHPUnit\Framework\assertEquals($post, $repository->findOne($post->uuid));
});

it("should throw a invalidPostDataException", function ($postData) {
    $repository = new InMemoryPostRepository();
    //$repository = new PDOPostRepository();
    $useCase = new CreatePost($repository);

    $post = $useCase->execute($postData);
})->with([
    [['title' => 'Mon titre', 'published_at' => new DateTime('2022-01-01')]],
    [['published_at' => new DateTime('2022-01-01')]],
    [[]],
])->throws(InvalidDataPostException::class);