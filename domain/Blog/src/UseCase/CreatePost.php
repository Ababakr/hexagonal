<?php 

namespace Domain\Blog\UseCase;

use Assert\LazyAssertionException;
use Domain\Blog\Entity\Post;
use Domain\Blog\Exception\InvalidDataPostException;
use Domain\Blog\Port\PostRepositoryInterface;
use function Assert\lazy;

class CreatePost {

    protected PostRepositoryInterface $repository;

    public function __construct(PostRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function execute(array $postData) : ?Post
    {
        $post = new Post($postData['title'] ?? '', $postData['content'] ?? '', $postData['publishedAt'] ?? null);

        try {
            $this->validate($post);
            $this->repository->save($post);

            return $post;

        } catch (LazyAssertionException $exception) {
            throw new InvalidDataPostException($exception->getMessage());
        }

    }

    protected function validate(Post $post)
    {
        lazy()->that($post->title)->notBlank()->minLength(3)
        ->that($post->content)->notBlank()->minLength(10)
        ->that($post->publishedAt)->nullOr()->isInstanceOf(\DateTimeInterface::class)
        ->verifyNow();
    }

}
