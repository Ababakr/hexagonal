<?php

namespace Domain\Blog\Entity;

use DateTimeInterface;

class Post
{
    public string $title;
    public string $content;
    public ?DateTimeInterface $publishedAt;
    public ?string $uuid;

    public function __construct(string $title = '', string $content = '', ?DateTimeInterface $publishedAt, ?string $uuid= null)
    {
        $this->uuid = $uuid ?? uniqid();
        $this->title = $title;
        $this->content = $content;
        $this->publishedAt = $publishedAt;
    }

}